# Universal Time Beats

The current basis of time measurment is very closely tied to the physical properties of the planet Earth. The Coordinated Universal Time (UTC) adopted standard includes leap years and leap seconds to keep in sync with Earth's movement through space. As humans become an interplanetary species, or spend more time in virtual worlds communicating with other humans in different time zones, the binding time to Earth becomes less ideal.

This repository explores the idea of a new time standard, built from the ground up with the goal of having as few fundamental ties to the planet Earth as possible, and instead base on universal natural forces, and human physiology.

At its core, this new time system creates a new fundamental unit of time different from a "second" to base all other measurements on. The definiton of a second worked backward from human observation, and got definitively described in 1967 as:

> The duration of 9,192,631,770 periods of the radiation corresponding to the transition between the two hyperfine levels of the ground state of the caesium-133 atom

The number of periods is not a nice, round number when expressed in decimal units. For this new time system, we define a new fundamental unit of time (a ***beat***) as being:

> The duration of 8,000,000,000 periods of the radiation corresponding to the transition between the two hyperfine levels of the ground state of the caesium-133 atom

This makes one beat slightly faster than one second of time. To convert between these time systems:

* 1.00 beat equals 8,000,000,000/9,192,631,770 = 0.87026220566213325000833792780106104478500176016514 seconds
* 1.00 second equals 9,192,631,770/8,000,000,000 = 1.14907897125 beats